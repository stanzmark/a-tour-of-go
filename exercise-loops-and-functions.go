package main

import (
	"fmt"
	"math"
	"os"
	"strconv"

	"github.com/pborman/getopt"
)

var (
	program = "exercise-loop-and-functions"
	version = "1.0.0"
)

func Sqrt(x float64, n int) float64 {
	z := 1.0
	y := 0.0
	for i := 0; i < n; i++ {
		z -= (z*z - x) / (2 * z)
		if y == z {
			break
		}
		y = z
	}
	return z
}

func main() {
	var (
		justVersion bool
		showHelp    bool
		useMath     bool
		cycles      = 10
	)

	getopt.BoolVarLong(&justVersion, "version", 0, "print version and exit")
	getopt.BoolVarLong(&showHelp, "help", 0, "show help")
	getopt.BoolVarLong(&useMath, "use-math", 'm', "use math.Sqrt")
	getopt.IntVarLong(&cycles, "iterations", 'i', "number of cycles", "integer")
	getopt.SetParameters("number")
	getopt.SetProgram(program)
	getopt.Parse()

	if justVersion {
		fmt.Printf("sqrt example %s\n", version)
		os.Exit(0)
	}

	if showHelp {
		getopt.PrintUsage(os.Stdout)
		os.Exit(0)
	}

	if len(os.Args) < 2 {
		getopt.PrintUsage(os.Stdout)
		os.Exit(1)
	}

	n, err := strconv.ParseFloat(getopt.Arg(0), 64)
	if err != nil {
		fmt.Println("Cannot convert to float64:", err)
	}

	fmt.Printf("Iterated Sqrt() %d times and got: %v\n", cycles, Sqrt(n, cycles))
	if useMath {
		fmt.Printf("Also called math.Sqrt and got: %v\n", math.Sqrt(n))
	}
}
