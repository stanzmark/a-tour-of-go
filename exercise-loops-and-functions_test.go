package main

import "testing"

func TestSqrt(t *testing.T) {
	sqrt := Sqrt(9, 10)
	if sqrt != 3.0 {
		t.Errorf("Sqrt is incorrect, got: %f, want: %f", sqrt, 3.0)
	}
}
